import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: Router, private fb: FormBuilder, private ds: DataService) { }
  loginForm: FormGroup;
  ngOnInit() {
    this.loginForm = this.fb.group(
      {
        username: ["", Validators.required],
        password: ["", Validators.required],
        remember: ""
      })
  }
  public loading = false;
  errorText = "";
  submittedFlag = true;
  //To check login credentials
  checkLogin() {
    this.submittedFlag = false;
    if (this.loginForm.valid) {

      var loginData = this.loginForm.value;
      if (loginData.username == "admin" && loginData.password == "admin") {
        this.loading = true;

        setTimeout(() => {
          this.loading = false;

          this.route.navigate(['/orders']);
          this.ds.loginDetails = loginData;
          if (loginData.remember) {
            this.ds.storeUserDetails(loginData);
          }
        }, 1000);

      } else {
        this.errorText = "Invalid Credentials";
      }

    }
  }

}
