import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retryWhen, delay, take } from 'rxjs/operators';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  loginDetails;


  //Storing userdetails in localStorage because of remember me in login page
  storeUserDetails(usrData) {
    localStorage.setItem("userDetails", JSON.stringify(usrData));
  }

  //Retreive user Details
  getUserDetails() {
    return JSON.parse(localStorage.getItem("userDetails"));
  }

  //Remove User Details
  removeUserDetails() {
    localStorage.clear();
  }

  // get and post requests for orders page
  getAllOrders() {
    return this.getRequest("getOrders")
  }
  createOrders(ordData) {
    return this.postRequest("postOrder", ordData)
  }
  editOrders(ordData) {
    return this.postRequest("editOrder", ordData);
  }
  deleteOrders(ordData) {
    return this.postRequest("remove", ordData);
  }

  //Get and Post Requests and also making use of  retryWhen,delay and take rxjs operators

  //Error Log code has been commented  

  getRequest(getUrl): Observable<any> {
    try {
      return this.http.get(getUrl).pipe(
        catchError(this.handleError),
        retryWhen(errors => errors.pipe(delay(3000), take(3)))
      );
    } catch (error) {

      // this.ErrorLog(error + '<span style="color:green"> in getRequest() </span> in <span style="color:red"> ( Data-Service ) </span>').subscribe(a => {
      // })

    }
  }


  postRequest(postUrl, postObj): Observable<any> {



    try {
      return this.http.post(postUrl, postObj);
    } catch (error) {

      // this.ErrorLog(error + '<span style="color:green"> in postRequest() </span> in <span style="color:red"> ( Data-Service ) </span>').subscribe(a => {
      // })

    }
  }
  handleError(error: HttpErrorResponse) {
    try {

      return throwError(error);

    } catch (error) {
      // this.ErrorLog(error + '<span style="color:green"> in handleError() </span> in <span style="color:red"> ( Data-Service ) </span>').subscribe(a => {
      // })

    }
  }
  // ErrorLog(errorMsg) {
  //   try {

  //     var date = new Date();


  //     var erMsg = errorMsg + "  on  " + date + "<br><br>";
  //     var ee = {
  //       data: erMsg
  //     };
  //     return this.postRequest(`PostErrorLog`, ee);
  //   } catch (error) {
  //     this.ErrorLog(error + '<span style="color:green"> in ErrorLog() </span> in <span style="color:red"> ( Data-Service ) </span>').subscribe(a => {
  //     })

  //   }
  // }

}


