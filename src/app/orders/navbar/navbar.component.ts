import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private ds: DataService, private router: Router) { }

  ngOnInit() {
  }
  logout() {
    this.router.navigate(["/login"]);
    this.ds.loginDetails = null;

    this.ds.removeUserDetails();
  }

}
