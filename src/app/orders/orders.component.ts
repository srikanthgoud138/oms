import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  constructor(private ds: DataService, private fb: FormBuilder) { }

  newOrderData: FormGroup;
  editOrderData: FormGroup;
  removeOrderData: FormGroup;
  orderList = [];
  loading = false;
  submitttedFlag = false;
  ngOnInit() {
    this.getOrders();
  }
  snackBarText = "";
  //To get newOrderForm Group controls
  getAddOrder() {
    this.submitttedFlag = true;
    this.newOrderData = this.fb.group({
      name: ["", Validators.required],
      dueDate: ["", Validators.required],
      address: ["", Validators.required],
      phone: ["", Validators.required],
      total: ["", Validators.required],
    });
  }
  //To create order
  createOrder() {
    this.submitttedFlag = false;
    if (this.newOrderData.valid) {
      try {

        this.newOrderData.value.dueDate = new Date(this.newOrderData.value.dueDate).getTime();
        this.newOrderData.value.CDATE = new Date().getTime();
        this.ds.createOrders(this.newOrderData.value).subscribe(a => {
          if (a.Status == "Success") {
            this.getOrders();
            $('#createOrder').modal('hide');
            this.snackBarText = "Order Created Successfully";
            this.showSnackBar();
          } else {
            $('#createOrder').modal('hide');
            this.snackBarText = "Something went wrong";
            this.showSnackBar();
          }
        })

      } catch (error) {
        $('#createOrder').modal('hide');
        this.snackBarText = "Something went wrong";
        this.showSnackBar();
      }

    }
  }

  showSnackBar() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }
  //To get all orders
  getOrders() {
    try {
      this.loading = true;
      this.ds.getAllOrders().subscribe(a => {
        this.loading = false;
        if (a.Status == "Success") {
          this.orderList = a.Data;
        }
      })
    } catch (error) {

      this.loading = false;
      this.orderList = [];
    }
  }

  //To get editOrderData Form Group controls
  getEditOrder(orderData) {
    this.submitttedFlag = true;
    this.editOrderData = this.fb.group({
      name: [orderData.name, Validators.required],
      dueDate: [orderData.dueDate, Validators.required],
      address: [orderData.address, Validators.required],
      phone: [orderData.phone, Validators.required],
      total: [orderData.total, Validators.required],
      ordernumber: orderData.ordernumber
    });

  }

  //To edit order
  editOrder() {
    this.submitttedFlag = false;
    if (this.editOrderData.valid) {
      try {


        this.editOrderData.value.dueDate = new Date(this.editOrderData.value.dueDate).getTime();
        this.ds.editOrders(this.editOrderData.value).subscribe(a => {
          if (a.Status == "Success") {
            $('#editOrder').modal('hide');
            this.getOrders();
            this.snackBarText = "Order Updated Successfully";
            this.showSnackBar();
          }
          else {
            $('#editOrder').modal('hide');
            this.snackBarText = "Something went wrong";
            this.showSnackBar();
          }
        })
      } catch (error) {
        console.log("11111");
        
        $('#editOrder').modal('hide');
        this.snackBarText = "Something went wrong";
        this.showSnackBar();
      }
    }
  }

  //To get removeOrderData Form Group Controls
  getRemoveOrder(orderData) {

    this.removeOrderData = this.fb.group({
      name: [orderData.name, Validators.required],
      dueDate: [orderData, Validators.required],
      address: [orderData.address, Validators.required],
      phone: [orderData.phone, Validators.required],
      total: [orderData.total, Validators.required],
      ordernumber: orderData.ordernumber
    });
  }

  //To remove order
  removeOrder() {
    try {

      this.ds.deleteOrders(this.removeOrderData.value).subscribe(a => {
        if (a.Status == "Success") {
          this.getOrders();
          $('#removeOrder').modal('hide');
          this.snackBarText = "Order removed Successfully";
          this.showSnackBar();
        } else {
          $('#removeOrder').modal('hide');
          this.snackBarText = "Something went wrong";
          this.showSnackBar();
        }

      });

    } catch (error) {
      $('#removeOrder').modal('hide');
      this.snackBarText = "Something went wrong";
      this.showSnackBar();
    }
  }
}
