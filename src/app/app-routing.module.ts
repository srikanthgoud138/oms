import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { CanActivateService } from './guards/can-activate.service';
import { CanLoadService } from './guards/can-load.service';
import { PnfComponent } from './pnf/pnf.component';


const routes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  { path: "login", component: LoginComponent, canActivate: [CanActivateService] },
  { path: "orders", loadChildren: "./orders/orders.module#OrdersModule", canLoad: [CanLoadService] },
  { path: "**", component: PnfComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
