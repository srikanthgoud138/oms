import { TestBed } from '@angular/core/testing';

import { CanLoadService } from './can-load.service';

describe('CanLoadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CanLoadService = TestBed.get(CanLoadService);
    expect(service).toBeTruthy();
  });
});
