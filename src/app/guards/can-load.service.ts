import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { DataService } from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class CanLoadService implements CanLoad {

  constructor(private ds: DataService, private router: Router) { }
  canLoad(route: Route): boolean {
    var locUserDetails = this.ds.getUserDetails();
    var serUserDetails = this.ds.loginDetails;
    if (locUserDetails != null || serUserDetails != null) {
      return true;
    } else {
      this.router.navigate(["/login"]);

      return false;
    }
  }
}
