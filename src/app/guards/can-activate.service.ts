import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { DataService } from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class CanActivateService implements CanActivate {

  constructor(private ds: DataService, private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    var locUserDetails = this.ds.getUserDetails();
    var serUserDetails = this.ds.loginDetails;
    if (locUserDetails != null || serUserDetails != null) {
      this.router.navigate(["/orders"]);
    }

    return true;
  }

}
