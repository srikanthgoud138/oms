const exp = require("express");
const app = exp();
var cor=require('cors');
app.use(cor({origin:'*'}));


const bodyParser = require("body-parser");
app.use(bodyParser.json()); 

const path = require("path");
app.use(exp.static(path.join(__dirname, "../dist/oms")));
  
const userRoutes=require('./Routes/userroute');

app.use('',userRoutes);


const port = 8081;

app.listen(port, () =>{
    console.log(`server connected to the port number ${port}`)
});
