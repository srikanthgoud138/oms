const exp = require("express");
const userRoutes = exp.Router();
const jwt = require("jsonwebtoken")


require("../db").connectToDb();
const getDBObj = require('../db').getDb;

// insert orders into orderlist collection

userRoutes.post('/postOrder', (req, res) => {
    try {

        getDBObj().collection("seqmaster").findOne({ name: "ORD" }, (err, result) => {
            if (err) {
                res.send({ Status: "Fail", Data: {}, Message: "Error while retreiving data" });
            } else {
                req.body.ordernumber = result.name + result.number++;
                getDBObj().collection("orderlist").insertOne(req.body, function (error, response) {
                    if (error) {
                        res.send({ Status: "Fail", Data: {}, Message: "Error while retreiving data" });
                    } else {
                        getDBObj().collection("seqmaster").updateOne({ name: "ORD" }, { $set: { number: result.number++ } }, (error1, uData) => {
                            if (error1) {
                                res.send({ Status: "Fail", Data: {}, Message: "Error while retreiving data" });
                            }
                            else {
                                res.send({ Status: "Success", Data: {}, Message: "Order inserted Successfully" });
                            }
                        });
                    }
                });
            }
        });

    } catch (ee) {
        // ErrorLog("==> Error in postOrder \r\n \r\t File Name = userroute.js \r\n \r\t==>Error Message = " + ee + " on @ " + new Date() + "\r\n");
        res.send({ Status: "Fail", Data: {}, Message: ee });
    }
})

// read orders from orderlist collection

userRoutes.get('/getOrders', (req, res) => {
    try {

        getDBObj().collection("orderlist").find({}).toArray((err, dataArray) => {
            if (err) {
                res.send({ Status: "Fail", Data: dataArray, Message: "Error while retreiving data" });
            }
            else if (dataArray.length > 0) {
                res.send({ Status: "Success", Data: dataArray, Message: "" });

            }
            else {
                res.send({ Status: "Success", Data: [], Message: "No Orders" });
            }
        })
    } catch (ee) {
        // ErrorLog("==> Error in getOrders \r\n \r\t File Name = userroute.js \r\n \r\t==>Error Message = " + ee + " on @ " + new Date() + "\r\n");
        res.send({ Status: "Fail", Data: {}, Message: ee });
    }

})

//deleting particular order details

userRoutes.post("/remove", (req, res) => {
    try {

        getDBObj().collection("orderlist").deleteOne({ ordernumber: req.body.ordernumber }, (err, deldata) => {
            if (err) {
                res.send({ Status: "Fail", Data: {}, Message: "Error while removing data" });

            }
            else {
                res.send({ Status: "Success", Data: {}, Message: "Order Updated Successfully" });

            }
        })

    } catch (ee) {
        // ErrorLog("==> Error in removeOrder \r\n \r\t File Name = userroute.js \r\n \r\t==>Error Message = " + ee + " on @ " + new Date() + "\r\n");
        res.send({ Status: "Fail", Data: {}, Message: ee });
    }
})

//updating paricular order details

userRoutes.post("/editOrder", (req, res) => {
    try {

        getDBObj().collection("orderlist").updateOne({ ordernumber: req.body.ordernumber }, {
            $set: {
                name: req.body.name,
                dueDate: req.body.dueDate,
                phone: req.body.phone,
                address: req.body.address,
                total: req.body.total,
                LastUpdatedDate:new Date().getTime()
            }
        }, (err, uData) => {
            if (err) {

                res.send({ Status: "Fail", Data: {}, Message: "Error while updating data" });
            }
            else {
                res.send({ Status: "Success", Data: {}, Message: "Order Updated Successfully" });

            }
        })

    } catch (ee) {
        // ErrorLog("==> Error in editOrder \r\n \r\t File Name = userroute.js \r\n \r\t==>Error Message = " + ee + " on @ " + new Date() + "\r\n");
        res.send({ Status: "Fail", Data: {}, Message: ee });
    }
})


// // user login    

// userRoutes.post("/login", (req, res) => {
//     console.log(req.body);
//     getDBObj().collection("userlogin").findOne({ username: req.body.username, password: req.body.password }, (err, result) => {
//         if (err) {
//             console.log("err in read")
//         }
//         else {
//             console.log(result);

//             jwt.sign({ username: result.username }, 'secret', (err, signedToken) => {
//                 if (err) {
//                     console.log("err in read")
//                 }
//                 else {
//                     res.json({ message: "User Logged In Successfully", token: signedToken })
//                 }
//             })
//         }
//     })
// })


module.exports = userRoutes;
